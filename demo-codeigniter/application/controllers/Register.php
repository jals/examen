<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$models	=	array('RegisterSave_model');
		$this->load->model($models);

	}


	public function setDataUsers()
	{
		$save =	new RegisterSave_model();


		if ($this->input->server('REQUEST_METHOD') == "POST")
		{
			$dataUser = array(
				'name'			=>	$this->input->post('nameUser'),
				'secondName'	=>	$this->input->post('secondNameUser'),
				'thirdName'		=>	$this->input->post('thirdNameUser'),
				'birthday'		=>	$this->input->post('dateCreate'),
				'generate'		=>	$this->input->post('gener'),
			);

			if ($dataUser['name']	==	Null || $dataUser['secondName']	==	NUll	||	$dataUser['birthday']	==	Null)
			{
					echo "Datos requeridos";
			}else{
				$save->saver_data_users($dataUser);

			}

		}

	}




	public function getDataUsers()
	{

		$getData	=	new RegisterSave_model();
		//$users	=	$getData->getUsers();
		$j=0;
		$k=0;
		$mayor	=	 18;



			if (is_array($getData->getUsers()) AND $getData->getUsers()	!= Null)
			{
					foreach ($getData->getUsers() as $user)
					{
						$data[$j++] = [
								'nombre'	=>	$user->name,
								'paterno'	=>	$user->secondName,
								'materno'	=>	$user->thirdName,
								'birth'		=>	$user->birthday,
								'sex'		=>	$user->generate,

						];



					}
						for ($i	=0; $i < count($data); $i++) {
							//var_dump($data[$i]['birth']);

							$birth = date('Y-m-j', strtotime($data[$i]['birth']));
							$totalAge = $this->age_calc($birth);
							if ($totalAge >= $mayor)
							{

								if ($data[$i]['sex'] != 1)
								{
									$sex = 'Mujer';

									$cupon	=	0;
								}else
								{
									$sex = "Hombre";
									$cupon	=	1;
								}


							}else
							{
								$cupon = 2;
							}

							$edades[$k++]	=	[
								'edad'		=>	$totalAge,
								'sex'		=>	$sex,
								'nombre'	=>	$data[$i]['nombre'],
								'patenro'	=>	$data[$i]['paterno'],
								'matenro'	=>	$data[$i]['materno'],
								'cupones'	=>	$cupon,

							];
						}
					#return $data;
					#$this->age_validate($edades);
					return $edades;

			}

	}




	public function age_calc($date)
	{
		list($y,$m,$d)	=	explode("-",$date);
		return (date("md") < $m.$d ? date("Y")-$y-1 : date("Y")-$y);
	}




	public function index()
	{

		$this->setDataUsers();

		$data = array(

			'registerUsers'	=>	$this->getDataUsers(),
		);


		$data = array(
			// Migas de pan
			'breadcums' => $this->load->view('templates/horizontal/main/breadcums_view', $data, TRUE),
			// Contenido dinámico
			'content_main' => $this->load->view('registers/register_view', $data, TRUE),
		);

		$data = array(
			// Tag title
			'title_lgral' => 'Registro de usuarios',
			// Archivos css
			'css' => FALSE,
			// Header de la página web
			'header' => $this->load->view('templates/horizontal/header_view', $data, TRUE),
			// Aside de la página web
			'aside' => $this->load->view('templates/horizontal/aside_view', $data, TRUE),
			// Menu de toda la paqgina
			'menu' => $this->load->view('templates/horizontal/menu_view', $data, TRUE),
			// Main de la página web
			'main' => $this->load->view('templates/horizontal/main_view', $data, TRUE),
			// Footer de la página de web
			'footer' => $this->load->view('templates/horizontal/footer_view', $data, TRUE),
			// Archivos JS
			'js' => FALSE

		);

		$this->load->view('layout_general', $data);
	}
}
