<?php


class RegisterSave_model extends CI_Model
{
	private $tbl_users;
	private $tbl_asign;

	public function __construct()
	{
		parent::__construct();
		$this->tbl_users 	= 	'users';
		$this->tbl_asign	=	'asignacion';
	}


	public function saver_data_users($data = false)
	{
		$this->db->insert($this->tbl_users, $data);
		//var_dump($data);
	//	$this->db->insert($this->tbl_asign);
	}




	private function get_data_table($where = FALSE, $obj = FALSE)
	{
		$this->db->select();

		$this->db->from($this->tbl_users);
//		$this->db->join();

		if ($where) {

			$this->db->where($where);
		}

		$query = $this->db->get();
		#	var_dump($query);

		if ($query->num_rows() > 0) {
			if ($obj) {

				return $query->row();
			} else {

				return $query->result();
			}
		} else {

			return FALSE;
		}
	}

	public function getUsers()
	{
		return	$this->get_data_table(false,0);
	}

}
