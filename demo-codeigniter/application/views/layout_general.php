<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title><?php echo (isset($title_lgral))? $title_lgral : '¡Hola!';?></title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >




	<?php
	if (isset($css) && is_array($css)) {
		foreach ($css as $css_key) {
			echo $css_key;
		}
	}
	?>

</head>
<body>
	<div class="hold-transition layout-top-nav">
		<div class="wrapper">

			<?php echo (isset($header))? $header : FALSE;?>

			<?php echo (isset($main))? $main : FALSE;?>

			<?php echo (isset($aside))? $aside : FALSE;?>

			<?php echo (isset($footer))? $footer : FALSE;?>
		</div>
		<!-- ./wrapper -->
	</div>




	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" ></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


	<?php
	if (isset($js) && is_array($js)) {
		foreach ($js as $js_key) {
			echo $js_key;
		}
	}
	?>

</body>
</html>
