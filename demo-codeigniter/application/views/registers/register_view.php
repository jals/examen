<div class="accordion" id="accordionExample">
	<div class="card">
		<div class="card-header" id="headingOne">
			<h2 class="mb-0">
				<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
						data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					Registro de usuarios
				</button>
			</h2>
		</div>

		<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
			<div class="card-body">
				<div class="container-fluid">

					<div class="row">


						<div class="col-md-12 col-8 col-12">
							<!-- Default box -->

							<?php echo form_open() ?>
							<div class="row">
								<div class="col">


									<!--Input Clave de elaborador-->
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">Nombre</span>
										</div>


										<?php
										$input_user_key = array(
												'type' => 'text',
												'id' => 'nameUser',
												'class' => 'form-control col-8',
												'data-toggle' => 'tooltip',
												'title' => '',
												'name' => 'nameUser',
												'placeholder' => 'Nombre'
										);
										echo form_input($input_user_key) ?>
									</div>


								</div>

								<div class="col">
									<!--Input Nomenclatura-->
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">Paterno</span>
										</div>

										<?php
										$input_user_key = array(
												'type' => 'text',
												'id' => 'secondNameUser',
												'class' => 'form-control col-8',
												'data-toggle' => 'tooltip',
												'title' => '',
												'name' => 'secondNameUser',
												'placeholder' => 'Apellido paterno'
										);
										echo form_input($input_user_key) ?>
									</div>
								</div>

								<div class="col">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">Materno</span>
										</div>

										<?php
										$input_user_key = array(
												'type' => 'text',
												'id' => 'thirdNameUser',
												'class' => 'form-control col-auto',
												'data-toggle' => 'tooltip',
												'title' => '',
												'name' => 'thirdNameUser',
												'placeholder' => 'Apellido materno'
										);
										echo form_input($input_user_key) ?>
									</div>

								</div>

							</div>




							<div class="row">
								<!--Fecha de cración-->
								<div class="col">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">Fecha nacimiento</span>
										</div>

										<?php
										$date_create = array(
												'type' => 'date',
												'id' => 'dateCreate',
												'class' => 'form-control',
												'data-toggle' => 'tooltip',
												'title' => '',
												'name' => 'dateCreate',
												'placeholder' => ''
										);
										echo form_input($date_create) ?>
									</div>
								</div>


								<!--Fecha de sesion-->
								<div class="col">
									<div class="input-group mb-4">
										<div class="input-group-prepend">
														<span class="input-group-text"
															  id="basic-addon1">Sexo</span>
										</div>

										<?php
										$dropdown_generate = array(
													'Seleccione su sexo',
													'Hombre',
													'Mujer',

										);
										echo form_dropdown('gener',$dropdown_generate,Null,'class="form-control"') ?>
									</div>
								</div>


							</div>




							<div class="row">

								<div class="col">
									<?php
									$btn_save_asign = array(
											'type' => 'submit',
											'content' => 'Guardar',
											'class' => 'btn btn-md btn-success col-md-12',
											'name' => 'actulizar',


									);
									echo form_button($btn_save_asign); ?>
								</div>

							</div>


							<?php echo form_close(); ?>

							<!-- /.card -->
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>

	<div class="card">
		<div class="card-header" id="headingTwo">
			<h2 class="mb-0">
				<button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
					Mostrar resultados
				</button>
			</h2>
		</div>
		<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
			<div class="card-body">
				<table class="table">
					<thead class="thead-dark">
					<tr>
						<th scope="col">Nombre</th>
						<th scope="col">Paterno</th>
						<th scope="col">Materno</th>
						<th scope="col">Edad</th>
						<th scope="col">Sexo</th>
						<th scope="col">Cupones</th>
					</tr>
					</thead>
					<tbody>

					<?php for ($i=0; $i < count($registerUsers);$i++):?>
						<?php #var_dump($registerUsers[$i]['edades']);?>
						<?php
							if ($registerUsers[$i]['cupones'] == 0 )
							{
								$cupon = "Tines un cuón de descuento para la estetica";
							}else if ($registerUsers[$i]['cupones'] == 1){
								$cupon = "Tines un cupón de descuento para la vinatería";
							}else if ($registerUsers[$i]['cupones'] == 2)
							{
								$cupon = "Tines un cupón para la feria";

							}
						?>
						<tr>
							<td><?php echo $registerUsers[$i]['nombre']?></td>
							<td><?php echo $registerUsers[$i]['patenro']?></td>
							<td><?php echo $registerUsers[$i]['matenro']?></td>
							<td><?php echo $registerUsers[$i]['edad']?></td>
							<td><?php echo $registerUsers[$i]['sex']?></td>
							<td><?php echo $cupon?></td>
						</tr>
					<?php endfor;?>

					</tbody>
				</table>

			</div>
		</div>
	</div>

</div>
